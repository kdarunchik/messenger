@extends('adminlte::page')

@section('title', 'Messenger')

@section('content_header')
    <button type="button" class="btn btn-shadow btn-wide btn-primary" onclick="location.href='{{ route('chat.create') }}'">
      <span class="btn-icon-wrapper pr-2 opacity-7">
        <i class="fa fa-plus fa-w-20"></i>
      </span>
      New chat
    </button>
@stop

@section('content')
  @if(session()->has('message'))
    <div class="alert alert-succes">
      {{ session()->get('message') }}
    </div>
  @endif
  @if($errors->any())
    <div class="alert alert-danger">
      <ul>
      @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
      </ul>
    </div>
  @endif
  <div class="d-flex flex-wrap justify-content-between">
  </div>
  <div class="card">
    <div class="card-header">
      <h3 class="card-title">Chats</h3>
    </div>
    <div class="card-body p-0">
      <table class="table table-striped projects">
        <thead>
          <tr>
            <th style="width: 1%">
              #
            </th>
            <th>
              Chat Title
            </th>
            <th style="width: 10%" 50%class="text-center">
              Unread messages
            </th>
            <th style="width: 10%" class="text-center">
            </th>
          </tr>
        </thead>
        <tbody>
        @foreach($chats as $chat)
          @can('view', $chat)
            <tr>
              <td>
                #
              </td>
              <td>
                <a href="{{ route('chat.view', ['id' => $chat['id']]) }}">
                  {{$chat->title}}
                </a>
                <br>
                <small>
                  Created by {{$chat->admin_chat->name}}
                </small>
              </td>
              <td class="project-state">
                <span data-toggle="tooltip" title="{{$countUnreadMessage[$chat->id]}} New Messages" class="badge badge-primary">{{$countUnreadMessage[$chat->id]}}</span>
              </td>
              <td>
                @can('edit', $chat)              
                  <button type="button" class="btn btn-block btn-info btn-sm" onclick="location.href='{{ route('chat.edit', ['id' => $chat['id']]) }}'">
                    <span class="btn-icon-wrapper pr-2 opacity-7">
                      <i class="fas fa-pencil-alt"></i>
                    </span>
                    Edit
                  </button>
                @endcan
              </td>
            </tr>
          @endcan
        @endforeach
        </tbody>
      </table>
    </div>
    <!-- /.card-body -->
  </div>
  <!-- /.card -->
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script>
    </script>
@stop