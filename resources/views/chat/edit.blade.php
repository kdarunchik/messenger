@extends('adminlte::page')

@section('title', 'Messenger')

@section('content_header')
  <h1>Edit Chat</h1>
@stop

@section('content')
  @if(session()->has('message'))
    <div class="alert alert-success">
      {{ session()->get('message') }}
    </div>
  @endif
  <div class="card">
    <div class="card-header">
       <h3 class="card-title">You can edit chat here</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      <form action="{{ route('chat.store') }}" enctype="multipart/form-data" method="post">
        {{ csrf_field() }}

        <div class="input-group mb-3">
          <div class="input-group-prepend">
            <div class="input-group-text">
              Chat Title
            </div>
          </div>
          <input type="text" name="title" class="form-control {{ $errors->has('title') ? 'is-invalid' : '' }}" value="{{ old('title') ?? $chat['title']}}" placeholder="" autofocus>
          @if ($errors->has('title'))
            <div class="invalid-feedback">
              <strong>{{ $errors->first('title') }}</strong>
            </div>
          @endif
        </div>
        
        <div class="form-group">
          <label>Participants</label>
          <select name="participants_id[]" id="participants_id" class="select2" multiple="multiple" class="form-control {{ $errors->has('participants_id') ? 'is-invalid' : '' }}" data-placeholder="Select Participants" style="width: 100%;">
            @foreach($users as $user)
              <option value="{{$user->id}}" @if((!empty(old('participants_id')) && in_array($user->id, old('participants_id'))) || (empty(old('participants_id')) && in_array($user->id, $selected_users))) selected @endif>
                {{$user->name}}
              </option>
            @endforeach
          </select>
        </div>

        @if ($errors->has('participants_id'))
          <div class="invalid-feedback">
            <strong>{{ $errors->first('participants_id') }}</strong>
          </div>
        @endif

        <button type="submit" class="btn btn-primary btn-block btn-flat">
          Save  
        </button>
        <input type="hidden" name="id" value="{{ $chat->id }}">
      </form>        
    </div>
    <!-- /.card-body -->
  </div>
  <!-- /.card -->
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script>
      $(function () {
      //Initialize Select2 Elements
      $('.select2').select2()
    })
    </script>
@stop