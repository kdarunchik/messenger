@extends('adminlte::page')

@section('title', 'Messenger')

@section('content_header')
@stop

@section('content')
  <div class="card direct-chat direct-chat-primary">
    <div class="card-header">
      <h3 class="card-title">{{$chat->title}}</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body h-100">
      <!-- Conversations are loaded here -->
      <div class="direct-chat-messages">
        @foreach($messages as $message)
        @if(Auth::id() !== $message->author_message->user->id)
        <!-- Message. Default to the left -->
        <div class="direct-chat-msg">
          <div class="direct-chat-infos clearfix">
            <span class="direct-chat-name float-left">{{$message->author_message->user->name}}</span>
            <span class="direct-chat-timestamp float-right">{{$message->updated_at}}</span>
          </div>
          <!-- /.direct-chat-infos -->
          <div class="direct-chat-text">
            {{$message->content}}
          </div>
          <!-- /.direct-chat-text -->
        </div>
        @elseif(Auth::id() == $message->author_message->user->id)
        <!-- /.direct-chat-msg -->
        <!-- Message to the right -->
        <div class="direct-chat-msg right">
          <div class="direct-chat-infos clearfix">
            <span class="direct-chat-name float-right">
              <div class="btn-group btn-group-sm align-self-center">
                <a onclick="editMessage({{$message['id']}})" class="btn btn-info"><i class="fas fa-pencil-alt"></i></a>
                <a href="{{ route('message.delete', ['id' => $message['id']]) }}" class="btn btn-danger"><i class="fas fa-trash"></i></a>
              </div>
            </span>
            <span class="direct-chat-timestamp float-left">
              {{$message->updated_at}}
              @if($message->unread_message_count == 0)<i class="fas fa-check-double"></i>
              @elseif($message->unread_message_count !== 0)<i class="fas fa-check"></i>
              @endif
            </span>
          </div>
          <!-- /.direct-chat-infos -->
          <div class="direct-chat-text" id="message_{{$message['id']}}">
            {{$message->content}}
          </div>
        </div>
          <!-- /.direct-chat-text -->
        <!-- /.direct-chat-msg -->
        @endif
        @endforeach
      </div>
      <!--/.direct-chat-messages-->
    </div>
    <!-- /.card-body -->
  
    <div class="card-footer">
      <form action="{{ route('message.store') }}" enctype="multipart/form-data" method="post">
        {{ csrf_field() }}

        <div class="input-group">
          <input type="text" name="message" id="message_input" placeholder="Type Message ..." class="form-control {{ $errors->has('message') ? 'is-invalid' : '' }}" value="">
          @if ($errors->has('message'))
            <div class="invalid-feedback">
              <strong>{{ $errors->first('message') }}</strong>
            </div>
          @endif
          <span class="input-group-append">
            <button type="submit" class="btn btn-primary">Send</button>
          </span>
          <input type="hidden" name="chat_id" value="{{ $chat->id }}">
          <input type="hidden" name="id" id="message_edit_id" value="">
        </div>
      </form>
    </div>
    <!-- /.card-footer-->
  </div>
  <!--/.direct-chat -->

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script>
      function editMessage(messageId)
        {
          let message = $("#message_"+messageId).html();
          $("#message_input").val(message);
          $("#message_edit_id").val(messageId);
        }
    </script>
@stop