<?php

use Illuminate\Database\Seeder;
use App\{User, Chat, ChatParticipant};
use Illuminate\Support\Arr;

class ChatsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::all();
        factory(Chat::class, 10)->create()->each(function ($chat) use ($users){
        	$participants_id = $users->except($chat->user_admin_id)->random(2)->modelKeys();
            $chat->users()->attach(array_merge([$chat->user_admin_id], $participants_id));
        });
    }
}
