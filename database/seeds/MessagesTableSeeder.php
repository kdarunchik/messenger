<?php

use Illuminate\Database\Seeder;
use App\{User, Chat, ChatParticipant, Message};

class MessagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Message::class, 50)->create()->each(function ($message) {
            $author = $message->author_message()->first();
            $participants_id = ChatParticipant::all()->except($author->id)->where("chat_id", $author->chat_id)->modelKeys();
            $message->unreadMessage()->attach($participants_id);
        });
    }
}
