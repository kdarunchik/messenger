<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Message extends Model
{
    protected $fillable = [
        'author_id', 'content', 
    ];

    public function unreadMessage()
    {
        return $this->belongsToMany('App\ChatParticipant','unread_messages', 'message_id', 'chat_participant_id')->withTimestamps();
    }

    public function author_message()
    {
        return $this->belongsTo('App\ChatParticipant', 'author_id', 'id');
    }

    public static function getAllChatMessages($id)
    { 
        $messages = Message::whereIn('author_id', ChatParticipant::where('chat_id', $id)->pluck('id')->toArray())->withCount('unreadMessage')->get();
        return $messages;
    }

    public static function deleteMessage($id)
    {
        $message = Message::find($id);
        $message->unreadMessage()->detach();
        $message->delete();
        return true;
    }

}
