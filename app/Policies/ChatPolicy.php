<?php

namespace App\Policies;

use App\{Chat, User};
use Illuminate\Auth\Access\HandlesAuthorization;

class ChatPolicy
{
    use HandlesAuthorization;

    public function view(User $user, Chat $chat)
    {
        return in_array($chat->id, $user->chats->modelKeys());
    }

    public function create(User $user)
    {
        return TRUE;
    }

    public function edit(User $user, Chat $chat)
    {
        return $user->id == $chat->user_admin_id;
    }
}
