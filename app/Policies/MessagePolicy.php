<?php

namespace App\Policies;

use App\{User, Message};
use Illuminate\Auth\Access\HandlesAuthorization;

class MessagePolicy
{
    use HandlesAuthorization;

    public function edit(User $user, Message $message)
    {
        return $user->id == $message->author_message->user->id;
    }

    public function delete(User $user, Message $message)
    {
        return $user->id == $message->author_message->user->id;
    }
}
