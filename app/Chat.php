<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Chat extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_admin_id', 'title', 
    ];

    public function users()
    {
        return $this->belongsToMany('App\User','chat_participants', 'chat_id', 'user_id')->withTimestamps();
    }

    public function admin_chat()
    {
        return $this->belongsTo('App\User', 'user_admin_id', 'id');
    }

    public static function getUsersChat($id)
    {
        return Chat::find($id)->users->modelKeys();
    } 

    public static function checkAccessByAdminChat($id)
    {
        return Chat::where("id", $id)->where("user_admin_id", Auth::id())->first();
    }

    public static function getCountUnreadMessageByUser()
    {
        $user = Auth::user();
        $chats = Chat::all();
        $countUnreadMessage = [];
        foreach($chats as $chat){
            if ($user->can('view', $chat)) {
                $userInChat = ChatParticipant::getUserInChat($chat['id'], $user['id']);
                $countUnreadMessage[$chat['id']] = UnreadMessage::where('chat_participant_id', $userInChat['id'])->count();
                }
        }
        return $countUnreadMessage;
    }

}
