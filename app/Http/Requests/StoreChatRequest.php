<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreChatRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'participants_id' => 'required|array|min:1'
        ];  
        if ($this->id) {
            $rules['title'] = 'required|string|min:3|max:250|unique:chats,title,' . $this->id;
        } else {
            $rules['title'] = 'required|string|min:3|max:250|unique:chats,title';
        }
        return $rules;
    }
}


