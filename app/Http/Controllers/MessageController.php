<?php

namespace App\Http\Controllers;

use App\{User, Chat, ChatParticipant, Message};
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\StoreMessageRequest;

class MessageController extends Controller
{
    
    public function store(StoreMessageRequest $request)
    {
        $participants_chat = ChatParticipant::getParticipantsChat($request->chat_id);
        $message = Message::updateOrCreate(
            ['id' => $request->id, 'author_id' => $participants_chat->where('user_id', Auth::id())->first()->id],
            ['content' => $request['message']]
        );
        $participants_unread_message = $participants_chat->except([$message->author_id])->modelKeys();
        $message->unreadMessage()->sync($participants_unread_message);
        return redirect()->back();
    }
        

    public function delete($id)
    {
        $message = Message::deleteMessage($id);
        return redirect()->back();
    }

}
