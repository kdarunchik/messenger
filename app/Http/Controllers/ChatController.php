<?php

namespace App\Http\Controllers;

use App\{User, Chat, ChatParticipant, Message};
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\StoreChatRequest;


class ChatController extends Controller
{    
    public function index()
    {
        $chats = Chat::all();
        $countUnreadMessage = Chat::getCountUnreadMessageByUser();
        return view(
            'home', 
            [
                "chats" => $chats,
                "countUnreadMessage" => $countUnreadMessage
            ]);
    }

    public function create()
    {
        $user = Auth::user();
        if ($user->can('create', Chat::class)) {
            $users = User::getAllNotUserAdminChat();
            return view('chat.create',["users" => $users]);
        } else {
            return redirect()->route("home");
        }
    }

    public function edit($id)
    {
        $user = Auth::user();
        $chat = Chat::find($id);
        if(empty($chat)){
            return redirect()->route('home')->withErrors(["Chat doesn't exists"]);
        }
        if ($user->can('edit', $chat)) {
            return view(
                'chat.edit',
                [
                    "chat" => $chat,
                    "users" => User::getAllNotUserAdminChat(),
                    "selected_users" => Chat::getUsersChat($id)
                ]);
        } else {
            return redirect()->route("home");
        }
    }

    public function store(StoreChatRequest $request)
    {
        $chat = Chat::updateOrCreate(
            ['id' => $request->id, 'user_admin_id' => Auth::id()],
            ['title' => $request['title']]
        );
        $chat->users()->sync(array_merge([$chat->user_admin_id], $request['participants_id']));
        return redirect()->route('home')->with(
            'message',
            "Chat #" . $chat->id . " \"" . $chat->title . "\" was saved!"
        );
    }

    public function view($id)
    {
        $user = Auth::user();
        $chat = Chat::find($id);
        $userInChat = ChatParticipant::getUserInChat($id, $user['id']);
        if ($user->can('view', $chat)) {
            $messages = Message::getAllChatMessages($id);
            $messages->each(function ($message) use ($userInChat){
                $message->unreadMessage()->detach($userInChat->id);
            });
            return view(
                'chat.view',
                [
                    "chat" => $chat,
                    "messages" => $messages
                ]);
        } else {
            return redirect()->route("home");
        }
    }

}
