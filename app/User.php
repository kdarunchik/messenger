<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function getAllNotUserAdminChat()
    {
        return User::where("id", "<>", Auth::id())->get();
    }

    public function chats()
    {
        return $this->belongsToMany('App\Chat','chat_participants', 'user_id', 'chat_id')->withTimestamps();
    }

    public function chats_with_admin()
    {
        return $this->hasMany('App\Chat', 'user_admin_id', 'id');
    }

}
