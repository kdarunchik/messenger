<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChatParticipant extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'chat_id', 'user_id',
    ];

    protected $touches = ['chat'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    public function chat()
    {
        return $this->belongsTo('App\Chat');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function message()
    {
        return $this->hasMany('App\Message', 'author_id', 'id');
    }

    public static function getParticipantsChat($id)
    {
        return ChatParticipant::all()->where("chat_id", $id);
    }

    public static function getUserInChat($chat_id, $user_id)
    {
        return ChatParticipant::where("chat_id", $chat_id)->where("user_id", $user_id)->first();
    }

}

