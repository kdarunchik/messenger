<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnreadMessage extends Model
{
    protected $fillable = [
        'message_id', 'chat_participant_id', 
    ];

    public function chat_participant()
    {
        return $this->belongsTo('App\ChatParticipant', 'id', 'chat_participant_id');
    }

    public function message()
    {
        return $this->belongsTo('App\Message', 'id', 'message_id');
    }
}
