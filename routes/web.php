<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::middleware(['auth'])->group(function () {
    Route::get('/', 'ChatController@index')->name('home');
    Route::prefix('chat')->group(function () {
        Route::get('/create', 'ChatController@create')->name('chat.create');
        Route::get('/edit/{id}', 'ChatController@edit')->name('chat.edit');
        Route::post('/store', 'ChatController@store')->name('chat.store');  
        Route::get('/{id}', 'ChatController@view')->name('chat.view');
    });
    Route::prefix('message')->group(function () {
    	Route::post('/store', 'MessageController@store')->name('message.store');
    	Route::get('/delete/{id}', 'MessageController@delete')->name('message.delete');
    });
});